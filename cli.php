<?php

function is_cli(){
    
    if (php_sapi_name() !== 'cli') {
        return false;
    }
    return true;
}


function ifset( $set ){
    if ( isset( $set ) && !empty( $set ) ) {
        return true;
    }
    return false;
}

require_once __DIR__ . '/Commands/Commands.php';

class CLI
{
    public $readline_support = false;
    public $wait_msg = 'Нажмите любую кнопку для продолжения';
    protected $initialized = false;

    protected $delim;
    protected $delim_options;

    public $command;
    public $arguments = [];
    public $options = [];
    public $description = '';
    protected $lastWrite;

    protected $commands;
    public $command_list = [];

    protected $escape = "   ";
    protected $dash = "-";
    
    public function init($argc, $argv){
        if (is_cli()){
            $this->delim = ','; // разделитель нескольких параметров
            $this->delim_options = '='; // разделитель нескольких параметров

            $this->readline_support = extension_loaded('readline');
            $this->parseCommandLine($argc, $argv);
            $this->commands = new Commands();
            $this->command_list = $this->commands->getCommands();
            $this->initialized = true;
        }else{
            define('STDOUT', 'php://output');
        }
    }

    public function newCommand($_argv){
        $argv[] = "cli";
        foreach ( explode(' ', $_argv) as $key => $value) {
            $argv[] = $value;
        }
        $argc = count( $argv );
        $this->parseCommandLine($argc, $argv);
    }

    
    protected function parseCommandLine($argc, $argv){
        
        $this->command = $argv[1];

        for ($i = 1; $i < $argc; $i++) {
            if (preg_match('/\[(.+?)\]/', $argv[$i], $match)){
                $this->inputOptions( $match );
            }
            if (preg_match('/^{(.+?)}/', $argv[$i], $match)){
                $this->inputArguments( $match );
            }
        }
    }

    
    protected function inputArguments($match){
        
        // если в значение аргумента есть разделитель
        if( strpos( $match[1], $this->delim ) !== false ){
            // преобразуем значение в массив из этих аргументов
            foreach (explode( $this->delim, $match[1] ) as $key => $value) {
                $this->arguments[] = $value;
            }
        }else{
            $this->arguments[] = $match[1];
        }

    }

    
    protected function inputOptions($argv){

        $_match = explode($this->delim_options, $argv[1]);
        if ( isset( $_match[1] ) ) {
            // регулярным выражением проверяем, есть ли нужное нам соответствие
            if (preg_match('/^{(.+?)}/', $_match[1], $match)){
                // если в значение аргумента есть разделитель
                if( strpos( $match[1], $this->delim ) !== false ){
                    // преобразуем значение в массив из этих аргументов
                    foreach (explode( $this->delim, $match[1] ) as $key => $value) {
                        $this->options[$_match[0]][] = $value;
                    }
                }else{
                    $this->options[$_match[0]][] = $match[1];
                }
            }else{
                $this->options[$_match[0]] = $_match[1];
            }
        }
        
    }

    public function description( $description ){
        $this->description = $description;
    }

    public function input(string $prefix = null): string{
        if ($this->readline_support)
        {
            return readline($prefix);
        }

        echo $prefix;

        return fgets(STDIN);
    }

    public static function prompt(string $field, $options = null, string $validation = null): string{
        $extra_output = ' ';

        self::fwrite(STDOUT, $field . $extra_output);

        return empty($input) ? '' : $input;
    }
    
    protected function fwrite($handle, string $string){
        if (is_cli()){
            fwrite($handle, $string);
            return;
        }

        echo $string;
    }
    public static function error(string $text){
        static::fwrite(STDERR, $text . PHP_EOL);
    }

    public static function newLine(int $num = 1){
        for ($i = 0; $i < $num; $i ++){
            static::fwrite( STDERR, PHP_EOL );
        }
    }

    public function saveCommand(){
        $this->commands->run( $this->command, [
            "arguments" => $this->arguments,
            "options" => $this->options,
            "description" => $this->description
        ] );

        $this->command_list = $this->commands->getCommands();

        $this->arguments = [];
        $this->options = [];
        $this->description = '';
    }

    public function getHelp(){
        $this->newLine();
        $this->prompt("Команда {LIST} покажет список всех комманд в системе");
        $this->newLine();
        $this->prompt("Команда {NEW} Добавит новую команду");
        $this->newLine();
        $this->prompt("Команда {EXIT} заканчивает программу");
        $this->newLine();
        $this->prompt("Команда command_name выведет данные команды");
        $this->newLine();
        $this->prompt("Команда command_name {HELP} покажет поисание определенной команды");
    }

    public function getCommands(){
        $this->newLine();
        $this->prompt("Список команд");
        foreach ($this->command_list  as $key => $value) {
            $this->newLine();
            $this->prompt($this->escape . $this->dash . $value['class']);
        }
    }

    public function getCommandHelp( $name ){
        $this->newLine();
        $this->prompt("Описание команды $name:");
        if( ifset($this->command_list[$name]) ){
            $this->newLine();
            $this->prompt($this->escape . $this->dash . $this->command_list[$name]['description']);
        }
    }

    public function infoCommand( $name ){
        if ( isset( $this->command_list[$name] ) ) {
            $this->newLine();
            $this->prompt("Called command: $name");
            if ( ifset( $this->command_list[$name]['arguments'] ) ) {
                $this->newLine();
                $this->prompt("Arguments: ");
                foreach ($this->command_list[$name]['arguments'] as $key => $value) {
                    $this->newLine();
                    $this->prompt($this->escape . $this->dash . " $value");
                }
            }
            
            if ( ifset( $this->command_list[$name]['options'] ) ) {
                $this->newLine();
                $this->prompt("Options: ");
                foreach ($this->command_list[$name]['options'] as $key => $value) {
                    if ( is_array( $value ) ) {
                        $this->newLine();
                        $this->prompt($this->escape . $this->dash . " $key");
                        foreach ($value as $key => $value2) {
                            $this->newLine();
                            $this->prompt($this->escape . $this->escape . $this->dash . " $value2");
                        }
                    }else{
                        $this->newLine();
                        $this->prompt($this->escape . $this->dash . " $key");
                        $this->newLine();
                        $this->prompt($this->escape . $this->escape . $this->dash . " $value");
                    }
                }
            }

        }else{
            $this->newLine();
            $this->prompt("Команда не найдена");
        }
    }

}
$cli = new CLI;
$cli->init($argc, $argv);
DESCRIPTION:
$cli->prompt("Хотите ввести описание для команды? (да/нет)");
//echo "Хотите ввести описание для команды? (да/нет)";

if($cli->input() != 'да'){
    goto REGISTR;
}
$cli->description( $cli->input() );
REGISTR:
$cli->saveCommand();

$cli->getHelp();

CLISWITCH:
$cli->newLine();
$cli->prompt("Введите команду");

$input = explode(' ', $cli->input());

if ( isset($cli->command_list[$input[0]] ) ) {
    $cli->newLine();
    $cli->prompt("Команда найдена");
    $cli->infoCommand( $input[0] );

    if ( ifset( $input[1] ) ) {
        
        switch ( strtoupper($input[1]) ) {
            case 'HELP':
            case '{HELP}':
                $cli->getCommandHelp($input[0]);
                goto CLISWITCH;
                break;

            case 'EXIT':
            case '{EXIT}':
                goto CLI_EXIT;
                break;
            default:
                $cli->prompt("Команда не определена");
                goto CLISWITCH;
                break;
        }

    }else{
        goto CLISWITCH;
    }
}
switch ( strtoupper($input[0]) ) {
    case 'LIST':
    case '{LIST}':
        goto CLIST;
        break;
    case 'HELP':
    case '{HELP}':
        goto CHELP;
        break;
    
    case 'NEW':
    case '{NEW}':
        goto CLI_NEW;
        break;
    case 'EXIT':
    case '{EXIT}':
        goto CLI_EXIT;
        break;
    default:
        $cli->prompt("Команда не определена");
        goto CLISWITCH;
        break;
}

CLIST:
$cli->getCommands();

goto CLISWITCH;


CHELP:
$cli->getHelp();

goto CLISWITCH;


CLI_NEW:
$cli->newLine();
$cli->prompt("Введите название команды, аргументы и опции:");
$cli->newLine();
$input = $cli->input();
$cli->newCommand($input);
goto DESCRIPTION;

CLI_EXIT: